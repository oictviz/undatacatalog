/**
* Unite Analytics Viz Template
* @author Kania Azrina | azrina@un.org
* UN Data Catalog Visualziation
* @author Yilin Wei | weiy@un.org
**/

/* Required for Qlik Sense integration with angular-based mashup */
define("client.services/grid-service", {});

/* Connect to a Qlik Server */
var config = {
    host: 'viz.unite.un.org',
     prefix: "/visualization/",
     port: null,
     isSecure: true
};

require.config({
    baseUrl:
        (config.isSecure ? "https://" : "http://")
        + config.host
        + (config.port ? ":" + config.port : "")
        + config.prefix + "resources"
});

require(["js/qlik"], function (qlik) {
    var qlikApp; //qlik App

    function getQlikApp() {
        return qlik.openApp("0e886feb-9668-42e5-a439-eb1b471a61b7", config); //app ID on Qlik server
    }

    /** MODULES **/
    var webApp = angular.module("webApp", ['ngRoute']);

    /** ROUTES **/
    webApp.config(function ($routeProvider) {
        $routeProvider
            .when("/", {templateUrl: "part/section1.html", controller: "vizCtrl"}) //controller
            // else 404
            .otherwise({templateUrl: "part/404.html", controller: "PageCtrl"});
    });
    
    

    /** CONTROLLERS **/
    webApp.controller("PageCtrl", function() {
        if (!qlikApp) {
            qlikApp = getQlikApp();
        }
    });

    webApp.controller("vizCtrl", ['$scope', '$location','$anchorScroll', function($scope, $location,$anchorScroll) {
        if (!qlikApp) {
            qlikApp = getQlikApp();
        }

        //selection bar
        qlikApp.getObject('CurrentSelections', 'CurrentSelections');
        //objects
        /* Activities and Committed Budget*/
        qlikApp.getObject('activity-kpi', chartsUN['activity-kpi']);
        qlikApp.getObject('budget-kpi', chartsUN['budget-kpi']);
        qlikApp.getObject('activity-year',chartsUN['activity-year']);
        qlikApp.getObject('budget-year',chartsUN['budget-year']);
        qlikApp.getObject('activity-status',chartsUN['activity-status']);
        qlikApp.getObject('budget-org-type',chartsUN['budget-org-type']);
        
        /* Top */
        qlikApp.getObject('top-recipients',chartsUN['top-recipients']);
        qlikApp.getObject('top-funding-org', chartsUN['top-funding-org']);
        qlikApp.getObject('top-implementing-org',chartsUN['top-implementing-org']);
        qlikApp.getObject('top-recipients-map', chartsUN['top-recipients-map']);
        
        /* Filters */
        qlikApp.getObject('filter-year',chartsUN['filter-year']);
        qlikApp.getObject('filter-reporting-org', chartsUN['filter-reporting-org']);
        qlikApp.getObject('filter-recipient', chartsUN['filter-recipient']);
        qlikApp.getObject('filter-funding',chartsUN['filter-funding']);
        qlikApp.getObject('filter-implementing', chartsUN['filter-implementing']);
        
        /* Budget */
        qlikApp.getObject('total-commitment', chartsUN['total-commitment']);
        qlikApp.getObject('total-disbursement',chartsUN['total-disbursement']);
        qlikApp.getObject('total-expenditure', chartsUN['total-expenditure']);
        qlikApp.getObject('total-incoming', chartsUN['total-incoming']);
        qlikApp.getObject('total-interest', chartsUN['total-interest']);
        qlikApp.getObject('total-loan', chartsUN['total-loan']);
        
        /* Table */
        qlikApp.getObject('table', chartsUN['table']);
        
        /* go to filter group */
       $scope.scrollTo = function(id) {
            $location.hash(id);
            $anchorScroll();
            };

    }])

    /** Bootstraping angular app for app, must be done before Qlik Sense API is used **/
    angular.bootstrap(document.documentElement, ["webApp", "qlik-angular"]);
    qlik.setOnError(function(error) {
        $("#errmsg").html(error.message).parent().show();
    });

});

